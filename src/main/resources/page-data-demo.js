
(function() {

    'use strict';

    // The data key is [groupId].[artifactId]:[web-resource-key].[data-key]
    var SIMPLE_DATA_KEY = 'com.atlassian.plugins.jira.page-data-demo:demo.simple-data';

    // Call WRM.data.claim() to retrieve your data.
    console.log(WRM.data.claim(SIMPLE_DATA_KEY));

    // Note, it's purged after the first call to WRM.data.claim(). If you want to use it multiple times, you'll need
    // to cache the variable yourself.
    console.log(WRM.data.claim(SIMPLE_DATA_KEY));

    // Logging a JSON data blob
    var GSON_DATA_KEY = 'com.atlassian.plugins.jira.page-data-demo:demo.gson-data';
    console.log(WRM.data.claim(GSON_DATA_KEY));


}());